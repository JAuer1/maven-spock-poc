# Maven-Spock-POC

A super simple, barebones application that runs Spock specifications as opposed to JUnit 4 test cases.

To run, just execute `mvn clean test`.

Please visit pom.xml for the dependencies and plugins needed.

This repo will also be utilized for a red/green/refactor LnL.

Please contact joshua.auer1@t-moblie.com with any questions.